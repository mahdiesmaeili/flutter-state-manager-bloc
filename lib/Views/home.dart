import 'package:bloce/Bloc/CounterBloc.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class HomeView extends StatelessWidget {
  const HomeView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Bloc'),
      ),
      body: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Padding(
            padding: const EdgeInsets.only(top: 20),
            child: Center(
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  FloatingActionButton(
                    onPressed: () {
                      //in dare kari mikone ke in button biad ro jaryan data ya hamon
                      //stream beshine bara hamin migan bloc ye stream dare
                      BlocProvider.of<CounterBloc>(context)
                          .add(Increment(payload: 1));
                    },
                    child: const Text("+"),
                  ),
                  const SizedBox(
                    width: 20,
                  ),
                  FloatingActionButton(
                    onPressed: () {
                      BlocProvider.of<CounterBloc>(context)
                          .add(Increment(payload: 2));
                    },
                    child: const Text("+2"),
                  ),
                  const SizedBox(
                    width: 20,
                  ),
                  FloatingActionButton(
                    onPressed: () {
                      BlocProvider.of<CounterBloc>(context)
                          .add(Increment(payload: 3));
                    },
                    child: const Text("R"),
                  ),
                ],
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(top: 40),
            child: Center(
              child: Text(
                'current number : ',
                style: Theme.of(context).textTheme.bodyText2,
              ),
            ),
          ),
          txt()
        ],
      ),
    );
  }

  Widget txt() {
    return Padding(
      padding: const EdgeInsets.only(top: 10),
      child: Center(
          child: BlocBuilder<CounterBloc, int>(
        //in javab nadad omad genericesh kard ta okay kar kone
        //bloc: CounterBloc(0),
        builder: (context, state) => Text(
          '$state',
          style: Theme.of(context).textTheme.bodyText1,
        ),
      )),
    );
  }
//todo Conventional method
// Widget txt() {
//   return Builder (
//     builder: (BuildContext context)=> Padding(
//       padding: const EdgeInsets.only(top: 10),
//       child: Center(child:
//        Text('0',style: Theme.of(context).textTheme.bodyText1,),
//
//       ),
//     ),
//   );
// }
}
