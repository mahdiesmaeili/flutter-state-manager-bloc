import 'dart:js';

import 'package:bloce/Bloc/CounterBloc.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'Views/home.dart';

void main() {
  runApp(MultiBlocProvider(
    child: const MyApp(),
    providers:[
      BlocProvider(create: (context) => CounterBloc(0) )
    ],
  ));
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: const HomeView(),
    );
  }
}

