import 'package:bloc/bloc.dart';

//in ravesh dovom estefade az class be jaye enum hastesh ke jalebe va karbordiii
abstract class CounterEvent{}

class Increment extends CounterEvent{
  final int payload;
  Increment({required this.payload});
}

class CounterBloc extends Bloc<CounterEvent, int>{
  CounterBloc(int initialState) : super(initialState){
    on<CounterEvent>((event,emit) async {
      switch (event.runtimeType){
        case Increment:
          emit(state+(event as Increment).payload);
          break;
      }
    });
  }
}












//todo in model bara zamani bood ke ma nakhiim az vorody to ui esteffade konim
//agar bekhaim esteffade konim az balali i esteffade mikonim
/*
//inja ma miaim se ta functionality ke darim ro miarim
//alan prose

enum CounterEvent { increment, incrementTwice, incrementR }

//in paiin bayad chizi ke bar migardone ro be ma neshon bede
class CounterBloc extends Bloc<CounterEvent, int> {
  // int aval yani vorodi avalmon int bashe
  CounterBloc(int initialState) : super(initialState) {
    // ye chizi darim be name on ke mishe bahash stae hamon ro control konim
    //va ba estefade az emit ona ro bargardonim
    on<CounterEvent>((event, emit) async{
      switch(event){
        case CounterEvent.increment:
          emit(state+1);
          break;
        case CounterEvent.incrementTwice:
          emit(state+2);
          break;
        case CounterEvent.incrementR:
          await Future.delayed(const Duration(seconds: 3));
          emit(state+5);
          break;
      }
    });
    //on jensi nadare in dobare miad ba generic behesh type ham mide ke betone vorodi
    //bahash set beshe
  }
}
*/